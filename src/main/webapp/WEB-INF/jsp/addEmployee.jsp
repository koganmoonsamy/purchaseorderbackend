<!DOCTYPE html>
<html>
<header>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700">
    <link rel="stylesheet" href="/css/common.css">
</header>
<body>

    <form:form name="AddEmployeeForm" action="/addEmployeePage" method="post" class="form-horizontal">
        <%@include file="Menu.jsp" %>
            <div  class="container-fluid">
                <div id=container class="col-md-offset-2 col-md-6">
                    <div  class="divPageName row">
                        <label class="col-sm-12">Add Employee Page</label>
                    </div>
                     <div class="divAddEmployee row">
                         <input type="text" name="employeeIdNumber" class="form-control col-sm-4" id="employeeID" placeholder="ID Number"/>
                         <label class= "col-sm-2"> </label>
                         <input type="text" name="firstName" class="form-control col-sm-8" id="firstName" placeholder="First Name"/>
                         <input type="text" name="secondName" class="form-control col-sm-8" id="secondName" placeholder="Second Name"/>
                         <input type="text" name="lastName" class="form-control col-sm-8" id="lastName" placeholder="Last Name"/>
                    </div>
                    <div class="divAddEmployee row">
                         <input type="text" name="addressLine1" class="form-control col-sm-12" id="addressLine1" placeholder="Address Line 1"/>
                         <input type="text" name="addressLine2" class="form-control col-sm-12" id="addressLine2" placeholder="Address Line 2"/>
                         <input type="text" name="addressLine3" class="form-control col-sm-12" id="addressLine3" placeholder="Address Line 3"/>
                    </div>
                    <div class="divAddEmployee row">
                         <input type="text" name="email" class="form-control col-sm-7" id="email" placeholder="Email"/>
                         <input type="text" name="cellNumber" class="form-control col-sm-6" id="cellNumber" placeholder="Cell Number"/>
                         <input type="text" name="jobDescription" class="form-control col-sm-10" id="jobDescription" placeholder="Job Description"/>
                    </div>
                    <div class="divAddEmployee row">
                         <button id="submitButton" type="button" class="btn btn-secondary btn-block col-sm-5" id="button">Submit</button>
                    </div>
                 </div>
            </div>
    </form:form>

</body>
</html>