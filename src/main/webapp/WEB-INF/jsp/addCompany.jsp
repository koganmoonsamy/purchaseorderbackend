<!DOCTYPE html>
<header>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700">
    <link rel="stylesheet" href="/css/common.css">
</header>
<body>

    <form:form name="addCompanyForm" action="/addCompanyPage" method="post" class="form-horizontal">
        <%@include file="Menu.jsp" %>
        <div  class="container-fluid">
            <div id=container class="col-md-offset-2 col-md-6">
                 <div  class="divPageName row">
                     <label class="col-sm-12">Add Company Page</label>
                  </div>
                 <div  class="divAddEmployee row">

                     <input type="text" name="companyName" class="form-control col-sm-8" id="companyName" placeholder="Company Name"/>
                     <label class="col-sm-4">  </label>
                     <input type="text" name="vatNumber" class="form-control col-sm-3" id="vatNumber" placeholder="VAT Number"/>
                     <label class="col-sm-9">  </label>
                     <input type="text" name="officeNumber" class="form-control col-sm-2" id="officeNumber" placeholder="Office Number"/>
                     <label class="col-sm-10">  </label>
                     <input type="text" name="email" class="form-control col-sm-6" id="email" placeholder="Email Address"/>

                 </div>
                 <div class="divAddEmployee row">
                     <input type="text" name="addressLine1" class="form-control col-sm-12" id="addressLine1" placeholder="Address Line 1"/>
                     <input type="text" name="addressLine2" class="form-control col-sm-12" id="addressLine2" placeholder="Address Line 2"/>
                     <input type="text" name="addressLine3" class="form-control col-sm-12" id="addressLine3" placeholder="Address Line 3"/>
                 </div>
                 <div class="divAddEmployee row">
                     <input type="text" name="contactPerson" class="form-control col-sm-6" id="contactPerson" placeholder="Contact Person Name"/>
                     <label class="col-sm-6">  </label>
                     <input type="text" name="cellNumber" class="form-control col-sm-5" id="cellNumber" placeholder="Contact Person Cell"/>
                 </div>
                 <div class="divAddEmployee row">
                     <button id="submitButton" type="button" class="btn btn-secondary btn-block col-sm-5" id="button">Submit</button>
                 </div>
             </div>
        </div>
    </form:form>

</body>
</html>