<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/common.css">
    <title>Main Menu</title>
</head>
<body id="login_body">
    <div class="container-fluid">
        <div >
            <img class="headerLogo" src="/images/CompanyLogo.png">
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="sidenav col-sm-3" >
                    <button onclick="dropdownList()" class="dropdown-btn">Employee</button>
                    <div class="dropdown-container" >
                        <a href="addEmployee" id="addEmployee">Add Employee</a>
                        <a href="queryEmployee" id="queryEmployee">Query Employee</a>
                    </div>
                    <button onclick="dropdownList()" class="dropdown-btn">Company Details</button>
                    <div class="dropdown-container">
                        <a href="addCompany">Add Company</a>
                        <a href="queryCompany">Query Company</a>
                    </div>
                    <button onclick="dropdownList()" class="dropdown-btn">Quotation</button>
                    <div class="dropdown-container">
                        <a href="createQuotation">Create Quotation</a>
                        <a href="queryQuotation">Query Quotation</a>
                    </div>
                    <button onclick="dropdownList()" class="dropdown-btn">Invoicing</button>
                    <div class="dropdown-container">
                        <a href="createInvoice">Create Invoice</a>
                        <a href="queryInvoice">Query Invoice</a>
                    </div>
                    <button onclick="dropdownList()" class="dropdown-btn">Mobile Repairs</button>
                    <div class="dropdown-container">
                        <a href="#">Create Mobile Repair Job Card</a>
                        <a href="#">Query Repair</a>
                    </div>
                    <button onclick="dropdownList()" class="dropdown-btn">Job Cards</button>
                    <div class="dropdown-container">
                        <a href="#">Create Job Card</a>
                        <a href="#">Query Job Card</a>
                        <a href="#">Edit Job Card</a>
                    </div>
                </div>
            </div>
            <div id="contentTest">

            </div>
        </div>
    </div>
 </body>
    
<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    function dropdownList()
    {
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) 
        {
          dropdown[i].addEventListener("click", function()
          {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") 
            {
              dropdownContent.style.display = "none";
            } 
            else 
            {
              dropdownContent.style.display = "block";
            }
          }
         );
        }
    }
    /*
    $(document).ready(function () {
        $("#queryEmployee").click(function (e) {
            $("#contentTest").load("queryEmployee");
        });
    });*/
    
</script>

</html>
