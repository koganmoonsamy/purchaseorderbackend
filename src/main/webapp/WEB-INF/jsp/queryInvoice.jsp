<!DOCTYPE html>
<html>
    <header>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700">
        <link rel="stylesheet" href="/css/common.css">
    </header>
    <body>
        <form:form name="queryInvoiceForm" action="/queryInvoicePage" method="post" class="form-horizontal">
            <%@include file="Menu.jsp" %>
            <div  class="container-fluid">
                <div id="container" class="col-md-offset-2 col-md-2">
                    <div  class="divPageName row">
                         <label class="col-sm-12">Query Invoice Page</label>
                    </div>
                    <div class="divAddEmployee row">
                        <input type="text" name="quotationID" class="form-control col-sm-12" id="quotationID" placeholder="Invoice ID"/>
                        <label class="col-sm-12"> </label>

                        <button id="submitButton" type="button" class="btn btn-secondary btn-block col-sm-5" id="button">Submit</button>
                    </div>
                    <div class="divAddEmployee row">

                    </div>
                </div>
            </div>
        </form:form>
    </body>
</html>