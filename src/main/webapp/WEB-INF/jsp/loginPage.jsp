<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700">
    <link rel="stylesheet" href="/css/common.css">

    <title>Login</title>

    <%-- CSRF-TOKEN --%>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body id="login_body">

<!-- main area -->
<div id="main-body">
    <!-- main-content -->
    <div id="main-login-content">
        <div class="container-fluid">
            <div>
                <img class="headerLogo" src="/images/CompanyLogo.png" >
            </div>
        </div>
            <div class="row page-title">
            </div><!--row-->
            <div class="sidenav" >
                <label>Login</label>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                        <div class="panel-body">

                            <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
                                <p class="help-block-error text-center"><c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/></p>
                            </c:if>

                            <form:form name="loginForm" action="/SecurityLoginPage" method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label for="loginIdEmail" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="username" class="form-control form-default-input" id="username" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="loginIdPassword" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" name="password" class="form-control default-form-input" id="password" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-secondary btn-block" id="loginIdButtonSubmit">Login</button>
                                    </div>
                                </div>
                            </form:form>

                        </div>

                </div>
            </div><!--row-->
        </div>
    </div><!--main-content-->
</div>

</body>
<footer>
    <!-- JAVASCRIPTS -->
    <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
</footer>

</html>
