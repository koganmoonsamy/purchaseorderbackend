<!DOCTYPE html>
<html>
<header>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700">
    <link rel="stylesheet" href="/css/common.css">
</header>
<body>

    <form:form name="createInvoiceForm" Form" action="/createInvoicePage" method="post" class="form-horizontal">
        <%@include file="Menu.jsp" %>
            <div  class="container-fluid">
                <div  class="divPageName col-md-offset-2 col-md-6">
                     <label class="col-sm-12">Create Quotation Page</label>
                </div>
                <div id=container class="col-md-offset-2 col-md-6">
                    <div class="divAddEmployee row">
                         <input type="text" name="date" class="form-control col-sm-5" id="date" placeholder="Date" onfocus="(this.type='date')" onblur="(this.type='text')"/>
                         <label class= "col-sm-7"> </label>
                         <input type="text" name="customerID" class="form-control col-sm-5" id="customerID" placeholder="Customer ID"/>
                         <label class= "col-sm-7"> </label>
                    </div>
                    <div class="divAddEmployee row">
                        <label class="col-sm-12">Customer Information</label>
                        <input type="text" name="name" class="form-control col-sm-4" id="name" placeholder="Name"/>
                        <label class= "col-sm-8"> </label>
                        <input type="text" name="addressLine1" class="form-control col-sm-6" id="addressLine1" placeholder="Address Line 1"/>
                        <label class= "col-sm-6"> </label>
                        <input type="text" name="addressLine2" class="form-control col-sm-6" id="addressLine2" placeholder="Address Line 2"/>
                        <label class= "col-sm-6"> </label>
                        <input type="text" name="addressLine3" class="form-control col-sm-6" id="addressLine3" placeholder="Address Line 3"/>
                        <label class= "col-sm-6"> </label>
                        <input type="text" name="email" class="form-control col-sm-7" id="email" placeholder="Email"/>
                        <input type="text" name="cellNumber" class="form-control col-sm-6" id="cellNumber" placeholder="Cell Number"/>

                    </div>
                    <div class="divAddEmployee row">
                         <label class="col-sm-12">Description</label>
                         <input type="text" name="description1" class="form-control col-sm-8" id="description1" placeholder="Description 1"/>
                         <label class="col-sm-1"> </label>
                         <input type="text" name="price1" class="form-control col-sm-2" id="price1" placeholder="Price"/>
                         <input type="text" name="description2" class="form-control col-sm-8" id="description2" placeholder="Description 2"/>
                         <label class="col-sm-1"> </label>
                         <input type="text" name="price2" class="form-control col-sm-2" id="price2" placeholder="Price"/>
                         <input type="text" name="description3" class="form-control col-sm-8" id="description3" placeholder="Description 3"/>
                         <label class="col-sm-1"> </label>
                         <input type="text" name="price3" class="form-control col-sm-2" id="price3" placeholder="Price"/>
                    </div>
                    <!--div class="divAddEmployee row">
                         <input type="text" name="email" class="form-control col-sm-7" id="email" placeholder="Email"/>
                         <input type="text" name="cellNumber" class="form-control col-sm-6" id="cellNumber" placeholder="Cell Number"/>
                         <input type="text" name="jobDescription" class="form-control col-sm-10" id="jobDescription" placeholder="Job Description"/>
                    </div-->
                    <div class="divAddEmployee row">
                         <button id="submitButton" type="button" class="btn btn-secondary btn-block col-sm-5" id="button">Submit</button>
                    </div>
                 </div>
            </div>
    </form:form>

</body>
</html>