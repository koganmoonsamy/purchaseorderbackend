package com.kagiso.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "quotation")
public class Quotation implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column (name = "quotation_id")
    private String quotationId;


}
