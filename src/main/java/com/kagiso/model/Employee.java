package com.kagiso.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by KoganM on 4/10/2018.
 */
@Entity
@Table (name = "employee")
public class Employee implements Serializable
{
    @Id
    @Column(name = "id_number")
    private String idNumber;

    @Column (name = "employee_number")
    private long employeeNumber;

    @Column (name = "first_name")
    private String firstName;

    @Column (name = "second_name")
    private String secondName;

    @Column (name = "last_name")
    private String lastName;

    @Column (name = "address_line1")
    private String addressLine1;

    @Column (name = "address_line2")
    private String addressLine2;

    @Column (name = "address_line3")
    private String addressLine3;

    @Column (name = "email")
    private String email;

    @Column(name = "cell_number")
    private String cellNumber;

    @Column (name = "job_description")
    private String jobDescription;

    public long getEmployeeNumber()
    {
        return employeeNumber;
    }

    public void setEmployeeNumber(long employeeNumber)
    {
        this.employeeNumber = employeeNumber;
    }

    public String getIdNumber()
    {
        return idNumber;
    }

    public void setIdNumber(String idNumber)
    {
        this.idNumber = idNumber;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getSecondName()
    {
        return secondName;
    }

    public void setSecondName(String secondName)
    {
        this.secondName = secondName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCellNumber()
    {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber)
    {
        this.cellNumber = cellNumber;
    }

    public String getJobDescription()
    {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription)
    {
        this.jobDescription = jobDescription;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getEmployeeNumber() == employee.getEmployeeNumber() &&
                Objects.equals(getIdNumber(), employee.getIdNumber()) &&
                Objects.equals(getFirstName(), employee.getFirstName()) &&
                Objects.equals(getSecondName(), employee.getSecondName()) &&
                Objects.equals(getLastName(), employee.getLastName()) &&
                Objects.equals(getAddressLine1(), employee.getAddressLine1()) &&
                Objects.equals(getAddressLine2(), employee.getAddressLine2()) &&
                Objects.equals(getAddressLine3(), employee.getAddressLine3()) &&
                Objects.equals(getEmail(), employee.getEmail()) &&
                Objects.equals(getCellNumber(), employee.getCellNumber()) &&
                Objects.equals(getJobDescription(), employee.getJobDescription());
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(getEmployeeNumber(), getIdNumber(), getFirstName(), getSecondName(), getLastName(), getAddressLine1(), getAddressLine2(), getAddressLine3(), getEmail(), getCellNumber(), getJobDescription());
    }
}
