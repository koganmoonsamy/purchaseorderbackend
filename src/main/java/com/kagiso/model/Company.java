package com.kagiso.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by KoganM on 4/10/2018.
 */
@Entity
@Table (name = "company")
public class Company implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Column (name = "company_id")
    private String companyId;

    @Column (name = "vat_number")
    private String vatNumber;

    @Column (name = "office_number")
    private String officeNumber;

    @Column (name = "company_name")
    private String companyName;

    @Column (name = "email")
    private String email;

    @Column (name = "address_line1")
    private String addressLine1;

    @Column (name = "address_line2")
    private String addressLine2;

    @Column (name = "address_line3")
    private String addressLine3;

    @Column (name = "Contact Person")
    private String contactPerson;

    @Column (name = "Cell Number")
    private String cellNumber;


    public String getVatNumber()
    {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber)
    {
        this.vatNumber = vatNumber;
    }

    public String getOfficeNumber()
    {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber)
    {
        this.officeNumber = officeNumber;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getContactPerson()
    {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson)
    {
        this.contactPerson = contactPerson;
    }

    public String getCellNumber()
    {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber)
    {
        this.cellNumber = cellNumber;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        Company company = (Company) o;
        return Objects.equals(getCompanyId(), company.getCompanyId()) &&
                Objects.equals(getVatNumber(), company.getVatNumber()) &&
                Objects.equals(getOfficeNumber(), company.getOfficeNumber()) &&
                Objects.equals(getCompanyName(), company.getCompanyName()) &&
                Objects.equals(getEmail(), company.getEmail()) &&
                Objects.equals(getAddressLine1(), company.getAddressLine1()) &&
                Objects.equals(getAddressLine2(), company.getAddressLine2()) &&
                Objects.equals(getAddressLine3(), company.getAddressLine3()) &&
                Objects.equals(getContactPerson(), company.getContactPerson()) &&
                Objects.equals(getCellNumber(), company.getCellNumber());
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(getCompanyId(), getVatNumber(), getOfficeNumber(), getCompanyName(), getEmail(), getAddressLine1(), getAddressLine2(), getAddressLine3(), getContactPerson(), getCellNumber());
    }
}
