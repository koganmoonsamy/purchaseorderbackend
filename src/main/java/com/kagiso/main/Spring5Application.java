package com.kagiso.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@SpringBootApplication
@ComponentScan(basePackages = {"com.kagiso.config",
                               "com.kagiso.controller",
                               "com.kagiso.main",
                               "com.kagiso.model",
                               "com.kagiso.repository",
                               "com.kagiso.service",
                               "com.kagiso.utils",
                               "com.kagiso.validator" })
public class Spring5Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Spring5Application.class, args);
    }
}
