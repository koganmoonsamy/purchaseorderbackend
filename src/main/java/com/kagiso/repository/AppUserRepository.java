package com.kagiso.repository;

import com.kagiso.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AppUserRepository extends JpaRepository<AppUser, Long>
{
    @Query(" select au from AppUser au where au.userName = ?1 ")
    AppUser findUserAccount(String userName);
}
