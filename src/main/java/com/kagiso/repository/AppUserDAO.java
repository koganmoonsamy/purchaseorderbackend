package com.kagiso.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.kagiso.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AppUserDAO
{
    @Autowired
    private EntityManager entityManager;

    public AppUser findUserAccount(String userName)
    {
        try
        {
            String sql = "Select e from " + AppUser.class.getName() + " e " //
                    + " Where e.userName = dbadmin1 ";

            Query query = entityManager.createQuery(sql);
            //query.setParameter("userName", userName);

            return (AppUser) query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

}
