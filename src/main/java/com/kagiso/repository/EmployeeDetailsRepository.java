package com.kagiso.repository;

import com.kagiso.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;


public interface EmployeeDetailsRepository extends JpaRepository<Employee, String>
{
    @Query("select emp.employeeNumber from Employee emp order by emp.employeeNumber desc")
    List<Long> getLastEmployeeNumber();
}
