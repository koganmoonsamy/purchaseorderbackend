package com.kagiso.repository;

import com.kagiso.model.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AppRoleRepository extends JpaRepository<AppRole, Long>
{
    @Query("Select ur.appRole.roleName from UserRole ur where ur.appUser.userId = ?1 ")
    List<String> getRoleNames(Long userId);

}
