package com.kagiso.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;


@Configuration
@PropertySource("classpath:config/kagisoTarp-database.properties")
@EnableJpaRepositories(
        entityManagerFactoryRef = "kagisoTarpEntityManagerFactory",
        transactionManagerRef = "kagisoTarpTransactionManager")

public class KagisoTarpDatasourceConfiguration
{
    @Autowired
    private Environment env;

    @Bean("kagisoTarpEntityManagerFactory")
    @ConfigurationProperties("kagisotarp-db.datasource")
    public LocalContainerEntityManagerFactoryBean kagisoTarpEntityManagerFactory()
    {
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(kagisoTarpJpaVendorAdaptor());
        localContainerEntityManagerFactoryBean.setDataSource(getDataSource());
        localContainerEntityManagerFactoryBean.setPersistenceUnitName("KagisoTarpPersistenceUnit");
        localContainerEntityManagerFactoryBean.setPackagesToScan("com.kagiso.model");
        localContainerEntityManagerFactoryBean.setJpaProperties(jpaProperties());
        localContainerEntityManagerFactoryBean.afterPropertiesSet();
        return localContainerEntityManagerFactoryBean;
    }

    @Bean("kagisoTarpDataSource")
    @ConfigurationProperties(prefix = "kagisotarp-db.datasource")
    public DataSource getDataSource()
    {
        return DataSourceBuilder.create().build();
    }


    @Bean("kagisoTarpJpaVendorAdaptor")
    public JpaVendorAdapter kagisoTarpJpaVendorAdaptor()
    {
        return new HibernateJpaVendorAdapter();
    }

    @Bean("kagisoTarpTransactionManager")
    public PlatformTransactionManager kagisoTarpTransactionManager()
    {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(
                kagisoTarpEntityManagerFactory().getObject());
        return jpaTransactionManager;
    }

    private Properties jpaProperties()
    {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));
        properties.put("hibernate.type", env.getRequiredProperty("hibernate.type"));
        properties.put("hibernate.sql", env.getRequiredProperty("hibernate.sql"));
        properties.put("hibernate.jdbc.lob.non_contextual_creation", "true");

        return properties;
    }

}
