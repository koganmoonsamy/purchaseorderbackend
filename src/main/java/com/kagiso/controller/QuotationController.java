package com.kagiso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class QuotationController
{
    @RequestMapping(value = "/createQuotation", method = RequestMethod.GET)
    public String createQuotationView()
    {
        return "createQuotation";
    }

    @RequestMapping(value = "/queryQuotation", method = RequestMethod.GET)
    public String queryQuotationView()
    {
        return "queryQuotation";
    }
}
