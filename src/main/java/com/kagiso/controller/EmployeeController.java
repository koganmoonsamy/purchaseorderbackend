package com.kagiso.controller;

import com.kagiso.model.Employee;
import com.kagiso.repository.EmployeeDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class EmployeeController
{
    //@Autowired
    //private EmployeeDetailsRepository employeeDetailsRepository;

    @RequestMapping(value = "/addEmployee", method = RequestMethod.GET)
    public String addEmployeeView()
    {
      return "addEmployee";
    }

    @RequestMapping(value = "/queryEmployee", method = RequestMethod.GET)
    public String queryEmployeeView()
    {
        return "queryEmployee";
    }

    /*
    @RequestMapping(value = "/addEmployeeJsp", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("employee") final Employee employee, final BindingResult result, final ModelMap model)
    {
        if (result.hasErrors())
        {
            return "error";
        }
        model.addAttribute("employeeNumber", employee.getEmployeeNumber());
        model.addAttribute("idNumber",employee.getIdNumber());
        model.addAttribute("firstName", employee.getFirstName());
        model.addAttribute("secondName", employee.getSecondName());
        model.addAttribute("lastName", employee.getLastName());
        model.addAttribute("addressLine1", employee.getAddressLine1());
        model.addAttribute("addressLine2", employee.getAddressLine2());
        model.addAttribute("addressLine3", employee.getAddressLine3());
        model.addAttribute("email", employee.getEmail());
        model.addAttribute("cellNumber", employee.getCellNumber());
        model.addAttribute("jobDescription", employee.getJobDescription());
        return "employeeView";
    }*/
/*
    @RequestMapping(value ="/addEmployee", method = RequestMethod.POST)
    @PostMapping("/addEmployee")
    public ModelAndView addEmployee ( Employee employee)
    {
        /*try
        {
            if (!employeeDetailsRepository.existsById(employee.getIdNumber()))
            {
                if (employeeDetailsRepository.getLastEmployeeNumber().size() == 0)
                {
                    /*This means that the employee table is empty so set
                      employee_number to 1

                    employee.setEmployeeNumber(1);
                }
                else
                {
                    employee.setEmployeeNumber(employeeDetailsRepository.getLastEmployeeNumber().get(0) + 1);
                }
                employeeDetailsRepository.save(employee);
            }

        }
        catch (DuplicateKeyException dke)
        {

        }
        catch (EntityNotFoundException e)
        {
            employeeDetailsRepository.save(employee);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Map<String, Employee> employeeMap = new HashMap<>();
        ModelMap model = new ModelMap();
        model.addAttribute("firstName", employee.getFirstName());

        employeeMap.put(employee.getIdNumber(), employee);

        return new ModelAndView("addEmployeeSuccess",employeeMap);*/
            //return new ModelAndView("");
   /* }

    @RequestMapping(value = "/message", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView sendMessage()
    {
        return new ModelAndView("message");
    }*/
}
