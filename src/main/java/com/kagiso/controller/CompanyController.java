package com.kagiso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CompanyController
{
    @RequestMapping(value = "/addCompany", method = RequestMethod.GET)
    public String addCompanyView()
    {
        return "addCompany";
    }

    @RequestMapping(value = "/queryCompany", method = RequestMethod.GET)
    public String queryCompanyView()
    {
        return "queryCompany";
    }
}
